package com.pdarrell;

import java.text.DecimalFormat;

public class DiskspaceFormatter {

    /**
     * Base code by Willem Van Onsem
     * @param space
     * @return
     */
    public static String calculateDiskSpaceString(double space)
    {

        if (space <= 0) {
            return "0";
        } else {
            String[] fileUnit = new String[] {"B","KB","MB","GB","TB"};
            int group = (int)(Math.log10(space) / Math.log10(1024));
            return new DecimalFormat("#,##0.#").format(space / Math.pow(1024, group)) + " " + fileUnit[group];
        }

    }

}
