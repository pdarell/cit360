package com.pdarrell;

public class Output {

    private static String outputString = "";


    public static String getOutputString() {
        return outputString;
    }

    public static void setOutputString(String output) {
        outputString += output;
    }

}
