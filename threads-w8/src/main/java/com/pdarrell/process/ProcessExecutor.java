package com.pdarrell.process;


import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ProcessExecutor {


    public static void processThread(Runnable thread)
    {
        ExecutorService exect = Executors.newFixedThreadPool(1);
        exect.execute(thread);
        try {
            exect.awaitTermination(60, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        exect.shutdownNow();

    }



}
