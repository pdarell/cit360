package com.pdarrell;

public class ListItem {


    private int index = 0;
    private String name = "";

    public int getIndex()
    {
        return index;
    }

    public void setIndex(int index)
    {
        this.index = index;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String toString()
    {
        return name + " name for index " + index;
    }


}
