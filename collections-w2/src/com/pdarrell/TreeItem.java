package com.pdarrell;

public class TreeItem implements Comparable {

    private int position = 0;
    private String title = "";
    private String description = "";


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String toString()
    {
        return "Position: " + this.getPosition() + ", Title: " + this.getTitle() + ", Description: " + this.getDescription();
    }



    public boolean equals(TreeItem compareItem)
    {
        if (this.getTitle().equals(compareItem.getTitle()))
        {
            return true;
        } else
        {
            return false;
        }
    }

    @Override
    public int compareTo(Object o) {
        if (this.getTitle().equals(((TreeItem)o).getTitle()))
        {
            return 1;
        } else
        {
            return 0;
        }
    }
}
