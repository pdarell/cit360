package com.pdarrell;

public class Collections {


    public static void processCollections()
    {
        runList();
        runSet();
        runQueue();
        runTree();
    }

    private static void runList()
    {
        ListCollection.createList();
        DisplayMessage.displayListCollection("Creating list...");

        Object item = ListCollection.findItem(2, ListCollection.returnCol());
        ListCollection.removeItemFromList(2);
        DisplayMessage.displayListCollection("Removing item " + item + " from list...");

        ListCollection.createListGeneric();
        DisplayMessage.displayListCollection("Creating list with generic...");

        Object item2 = ListCollection.findItem(3, ListCollection.returnCol());
        ListCollection.removeItemFromList(3);
        DisplayMessage.displayListCollection("Removing item " + item2 + " from list with generic...");

        ListCollection.sortList(true);
        DisplayMessage.displayListCollection("Sorting list reverse...");

        ListCollection.sortList(false);
        DisplayMessage.displayListCollection("Sorting list forward...");
    }

    private static void runSet()
    {
        SetCollection.createSet();
        DisplayMessage.displaySetCollection("Creating Set...");

        SetCollection.createSetAgain();
        DisplayMessage.displaySetCollection("Creating same Set again - duplicates not saved in set...");

        Object item = SetCollection.findItem("ItemNumber7", SetCollection.returnCol());
        SetCollection.removeItemFromSet("ItemNumber7");
        DisplayMessage.displaySetCollection("Removing item " + item + " from set...");

        Object item2 = SetCollection.findItem("ItemNumber3", SetCollection.returnCol());
        SetCollection.removeItemFromSet("ItemNumber3");
        DisplayMessage.displaySetCollection("Removing item " + item2 + " from set...");

    }

    private static void runQueue()
    {
        QueueCollection.createQueue();
        DisplayMessage.displayQueueCollection("Creating Queue...");

        QueueCollection.createQueueAgain();
        DisplayMessage.displayQueueCollection("Creating same Queue again - duplicates are at end of queue...");

        QueueCollection.createQueueNull();
        DisplayMessage.displayQueueCollection("Creating same Queue again - nulls are allowed in queue...");

        Object item = QueueCollection.findItem("ItemNumber7", QueueCollection.returnCol());
        QueueCollection.removeItemFromQueue("ItemNumber7");
        DisplayMessage.displayQueueCollection("Removing item " + item + " from queue...");

        Object item2 = QueueCollection.findItem("ItemNumber3", QueueCollection.returnCol());
        QueueCollection.removeItemFromQueue("ItemNumber3");
        DisplayMessage.displayQueueCollection("Removing item " + item2 + " from queue...");

    }

    private static void runTree()
    {
        TreeCollection.createTreeSetLower();
        DisplayMessage.displayTreeCollection("Creating TreeSet lower case...");

        TreeCollection.createTreeSet();
        DisplayMessage.displayTreeCollection("Creating TreeSet upper case - lower case sorted second...");

        TreeCollection.createTreeSetAgain();
        DisplayMessage.displayTreeCollection("Creating same TreeSet upper case again - duplicates are not allowed treeset...");



    }



}


