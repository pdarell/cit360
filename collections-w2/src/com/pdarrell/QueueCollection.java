package com.pdarrell;

import java.util.*;


public class QueueCollection {

    private static Queue<String> queueCol = new LinkedList<String>();

    public static void createQueue()
    {
        queueCol.clear();
        String root = "ItemNumber";
        for (int i = 0; i < 10; i++)
        {
            queueCol.add(root + i);
        }
    }

    public static void createQueueAgain()
    {
        String root = "ItemNumber";
        for (int i = 0; i < 10; i++)
        {
            queueCol.add(root + i);
        }
    }

    public static void createQueueNull()
    {
        String root = "ItemNumber";
        for (int i = 0; i < 10; i++)
        {
            if (i % 2 == 0)
            {
                queueCol.add(null);
            } else {
                queueCol.add(root + i);
            }
        }
    }

    public static void removeItemFromQueue(String entry)
    {
        int i = queueCol.size();
        while (queueCol.contains(entry))
        {
            queueCol.remove(entry);
        }
    }

    public static String findItem(String entry, Queue setcol)
    {
        Iterator iter = setcol.iterator();
        while (iter.hasNext())
        {
            Object obj = iter.next();
            if (obj != null) {
                String item = (String)obj;
                if (item.equals(entry)) {
                    return item;
                }
            }
        }

        return null;
    }

    public static Queue returnCol()
    {
        return queueCol;
    }


    public static void printCol(Queue inputSet)
    {
        for (Object item : inputSet)
        {
            System.out.println(item);
        }
    }


}
