package com.pdarrell;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ListCollection {

    private static List listCol = new ArrayList();

    public static void createList()
    {
        listCol.clear();
        String root = "ItemNumber";
        for (int i = 0; i < 10; i++)
        {
            listCol.add(root + i);
        }
    }

    public static void createListGeneric()
    {
        listCol.clear();
        String generic = "ListItemNumber";
        for (int i = 0; i < 10; i++)
        {
            ListItem item = new ListItem();
            item.setName(generic + i);
            item.setIndex(i);
            listCol.add(item);
        }
    }


    public static void removeItemFromList(int index)
    {
        listCol.remove(index);
    }


    public static void sortList(boolean reverse)
    {
        Collections.sort(listCol, new NameCompatorSort(reverse));
    }


    public static List returnCol()
    {
        return listCol;
    }


    public static void printCol(List inputList)
    {
        if (listCol instanceof ListItem)
        {
            for (Object item : inputList)
            {
                System.out.println(item);
            }
        } else
        {
            for (int i = 0; i < inputList.size(); i++)
            {
                System.out.println(inputList.get(i)) ;
            }
        }

    }

    public static Object findItem(int index, List listcol)
    {
        if (listCol instanceof ListItem)
        {
            for (Object item : listcol)
            {
                if (index == ((ListItem)item).getIndex())
                {
                    return (ListItem)item;
                }
            }
        } else
        {
            for (int i = 0; i < listcol.size(); i++)
            {
                if(i == index)
                {
                    return listcol.get(i);
                }
            }
        }
        return null;
    }




}
