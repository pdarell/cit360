package com.pdarrell;

import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

public class DisplayMessage {


    public static void displayListCollection(String message)
    {
        System.out.println(message);

        List outputList = ListCollection.returnCol();
        ListCollection.printCol(outputList);
    }


    public static void displaySetCollection(String message)
    {
        System.out.println(message);

        Set outputSet = SetCollection.returnCol();
        SetCollection.printCol(outputSet);
    }


    public static void displayQueueCollection(String message)
    {
        System.out.println(message);

        Queue outputQueue = QueueCollection.returnCol();
        QueueCollection.printCol(outputQueue);
    }

    public static void displayTreeCollection(String message)
    {
        System.out.println(message);

        TreeSet outputTree = TreeCollection.returnCol();
        TreeCollection.printCol(outputTree);
    }


}
