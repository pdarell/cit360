package com.pdarrell;

import javax.swing.text.html.HTMLDocument;
import java.util.*;
import java.util.Collections;

public class SetCollection {


    private static Set<String> setCol = new HashSet<String>();

    public static void createSet()
    {
        setCol.clear();
        String root = "ItemNumber";
        for (int i = 0; i < 10; i++)
        {
            setCol.add(root + i);
        }
    }

    public static void createSetAgain()
    {
        String root = "ItemNumber";
        for (int i = 0; i < 10; i++)
        {
            setCol.add(root + i);
        }
    }

    public static void removeItemFromSet(String entry)
    {
        setCol.remove(entry);
    }

    public static String findItem(String entry, Set setcol)
    {
        Iterator iter = setcol.iterator();
        while (iter.hasNext())
        {
            String item = (String)iter.next();
            if (item.equals(entry))
            {
                return item;
            }
        }

        return null;
    }


    public static Set returnCol()
    {
        return setCol;
    }


    public static void printCol(Set inputSet)
    {
        for (Object item : inputSet)
        {
            System.out.println(item);
        }
    }


}
