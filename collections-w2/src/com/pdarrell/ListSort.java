package com.pdarrell;

import java.util.Comparator;

public class ListSort implements Comparator<String>
{

    private boolean isReverse = false;

    public ListSort(boolean reverse)
    {
        this.isReverse = reverse;
    }

    @Override
    public int compare(String o1, String o2) {

        if (isReverse)
        {
            return o2.compareTo(o1);
        } else {
            return o1.compareTo(o2);
        }
    }


}
