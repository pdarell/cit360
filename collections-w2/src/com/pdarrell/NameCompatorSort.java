package com.pdarrell;

import java.util.Comparator;

public class NameCompatorSort implements Comparator<ListItem> {

    private boolean isReverse;

    public NameCompatorSort(boolean reverse)
    {
        this.isReverse = reverse;
    }

    @Override
    public int compare(ListItem item1, ListItem item2) {
        if (isReverse)
        {
            return item2.getName().compareTo(item1.getName());
        } else
        {
            return item1.getName().compareTo(item2.getName());
        }
    }


}
