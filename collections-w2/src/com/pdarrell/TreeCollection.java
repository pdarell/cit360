package com.pdarrell;

import java.util.TreeSet;


public class TreeCollection {


    private static TreeSet<String> treeCol = new TreeSet<String>();


    public static void createTreeSet()
    {
        String root = "ItemNumber";
        for (int i = 0; i < 10; i++)
        {
            TreeItem item = new TreeItem();
            item.setPosition(i);
            item.setTitle(root + i);
            item.setDescription("Description of " + root + i);
            treeCol.add(root + i);
        }
    }

    public static void createTreeSetAgain()
    {
        String root = "ItemNumber";
        for (int i = 0; i < 10; i++)
        {
            TreeItem item = new TreeItem();
            item.setPosition(i);
            item.setTitle(root + i);
            item.setDescription("Description of " + root + i);
            treeCol.add(root + i);
        }
    }

    public static void createTreeSetLower()
    {
        String root = "itemNumber";
        for (int i = 0; i < 10; i++)
        {
            TreeItem item = new TreeItem();
            item.setPosition(i);
            item.setTitle(root + i);
            item.setDescription("Description of " + root + i);
            treeCol.add(root + i);
        }
    }




    public static TreeSet returnCol()
    {
        return treeCol;
    }


    public static void printCol(TreeSet inputTreeSet)
    {
        for (Object item : inputTreeSet)
        {
            System.out.println(item);
        }
    }


}
