package com.pdarrell;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;

import java.io.IOException;

public class Server {



    public static void receiveComputerStatus(String jsonComputerStatus)
    {
        ObjectMapper objMapper = new ObjectMapper();
        Status status = null;
        Disk disk = null;
        if (jsonComputerStatus != null && !jsonComputerStatus.equals("")) {
            ComputerStatus computerStatus = null;
            try {
                computerStatus = objMapper.readValue(jsonComputerStatus, ComputerStatus.class);
            } catch (IOException e) {
                System.out.println(e.getMessage());
                e.printStackTrace();
            }
            if (computerStatus != null) {
                status = computerStatus.getStatus();
                disk = computerStatus.getDisk();
            }
        }

        if (status != null) {
            System.out.println(status);
        } else
        {
            System.out.println("No status information available.");
        }
        if (disk != null)
        {
            System.out.println(disk);
        } else
        {
            System.out.println("No disk information available.");
        }

    }



}
