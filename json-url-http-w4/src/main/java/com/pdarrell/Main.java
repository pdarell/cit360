package com.pdarrell;

import java.net.InetAddress;
import java.net.InetAddress.*;
import java.net.UnknownHostException;
import java.util.Date;

public class Main {

    public static void main(String[] args) {
        String computerName = "";
        try {
            computerName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        Server.receiveComputerStatus(Client.sendComputerStatus(
                Client.createComputerStatus(
                        computerName, "ACTIVE", new Date(), "c:", 8192 )));




    }



}
