package com.pdarrell;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Date;

public class Client {

    public static String sendComputerStatus(ComputerStatus computerStatus)
    {
        ObjectMapper objMapper = new ObjectMapper();
        String returnValue = "";
        try {
            returnValue = objMapper.writeValueAsString(computerStatus);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    public static ComputerStatus createComputerStatus(String computerName, String statusInd, Date datetime, String drive, long space, boolean inActive)
    {
        Status status = null;
        Disk disk = null;
        Computer computer = null;
        if (!computerName.equals(""))
        {
            computer = createComputer(computerName, inActive);
        }
        if (!computerName.equals("") && !statusInd.equals("") && datetime != null) {
            status = createStatus(computer, statusInd, datetime);
        }
        if (!computerName.equals("") && !drive.equals("")) {
            disk = createDisk(computer, drive, space, datetime);
        }
        ComputerStatus computerStatus = new ComputerStatus();
        computerStatus.setStatus(status);
        computerStatus.setDisk(disk);

        return computerStatus;

    }

    private static Status createStatus(Computer computer, String statusInd, Date datetime)
    {
        Status status = new Status();
        status.setComputer(computer);
        status.setStatusIndicator(statusInd);
        status.setScanDateTime(datetime);
        return status;
    }

    private static Disk createDisk(Computer computer, String drive, long space, Date datetime)
    {
        Disk disk = new Disk();
        disk.setComputer(computer);
        disk.setDrive(drive);
        disk.setFreeDiskspace(space);
        disk.setScanDateTime(datetime);
        return disk;
    }

    private static Computer createComputer(String computerName, boolean inActive)
    {
        Computer computer = new Computer();
        computer.setComputerName(computerName);
        computer.setInActive(inActive);
        return computer;
    }

}
