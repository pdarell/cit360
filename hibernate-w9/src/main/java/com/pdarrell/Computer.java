package com.pdarrell;

import com.pdarrell.dao.ComputerDAO;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="Computer")
public class Computer {

    @Id
    @Column(name = "Computer_id")
    private long computerId = getMaxId() + 1;

    @Column(name = "ComputerName")
    private String computerName = "";

    @Column(name = "InActive")
    private boolean inActive = false;

  /*  @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL,CascadeType.PERSIST,CascadeType.MERGE }, mappedBy = "parent")
    @Column(nullable = false)
    private List<Status> statuses = null;

    @OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL,CascadeType.PERSIST,CascadeType.MERGE }, mappedBy = "parent")
    @Column(nullable = false)
    private List<Disk> disks = null;
*/
    public long getComputerId() {
        return computerId;
    }

    public void setComputerId(long computerId) {
        this.computerId = computerId;
    }

    public String getComputerName() {
        return computerName;
    }

    public void setComputerName(String computerName) {
        this.computerName = computerName;
    }

    public boolean isInActive() {
        return inActive;
    }

    public void setInActive(boolean inActive) {
        this.inActive = inActive;
    }

    private long getMaxId()
    {
        return ComputerDAO.getMaxId();
    }

/*
    public List<Status> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<Status> statuses) {
        this.statuses = statuses;
    }

    public List<Disk> getDisks() {
        return disks;
    }

    public void setDisks(List<Disk> disks) {
        this.disks = disks;
    }
*/

}
