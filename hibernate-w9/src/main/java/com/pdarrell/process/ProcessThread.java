package com.pdarrell.process;

import com.pdarrell.Client;
import com.pdarrell.Output;
import com.pdarrell.Server;

import java.util.Date;

public class ProcessThread implements Runnable{

    private boolean exit = false;
    private String computerName = "";
    private String outputString = "";
    private String drive = "";
    private long diskspace = 0;
    private OutputThread outputThread = null;



    public ProcessThread (String computerName, String drive, long diskspace, OutputThread outputThread)
    {
        this.computerName = computerName;
        this.drive = drive;
        this.diskspace = diskspace;
        this.outputThread = outputThread;
    }


    @Override
    public void run() {

        while (!exit)
        {
            Output.setOutputString(Server.receiveComputerStatus(Client.sendComputerStatus(
                    Client.createComputerStatus(
                            computerName, "ACTIVE", new Date(), drive, diskspace, false))));
           //outputThread.createInstance();
           Thread.yield();
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        outputThread.setExit(true);
    }


    public String getOutputString() {
        return outputString;
    }


    public void setExit(boolean exit)
    {
        this.exit = exit;
    }



}
