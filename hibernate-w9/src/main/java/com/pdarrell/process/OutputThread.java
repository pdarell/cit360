package com.pdarrell.process;

import com.pdarrell.Output;

public class OutputThread implements Runnable {

    private boolean exit = false;
    private String outputString = "";
    private OutputThread instance = null;


    public OutputThread()
    {
        Thread thread = new Thread(this);
        thread.start();
    }


    public OutputThread createInstance()
    {
        if (instance == null)
        {
            return new OutputThread();
        } else
        {
            return this;
        }
    }



    @Override
    public void run() {

        while (!exit) {

            outputString = Output.getOutputString();
            Thread.yield();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public String getOutputString() {
        return outputString;
    }


    public void setExit(boolean exit)
    {
        this.exit = exit;
    }

}
