package com.pdarrell.bean;

import com.pdarrell.Output;
import com.pdarrell.process.OutputThread;
import com.pdarrell.process.ProcessExecutor;
import com.pdarrell.process.ProcessThread;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class ComputerStatusBean {
    private String computerName = "";
    private String outputInfo = "";
    private String drive = "";
    private long diskspace = 0;
    private OutputThread outputThread = new OutputThread();
    private ProcessThread processThread = null;


    public void requestInfo()
    {
        FacesContext  context = FacesContext.getCurrentInstance();

        if (this.computerName.equals(""))
        {
            String messageText = "Must enter a computer name.";
            context.addMessage(messageText, new FacesMessage(messageText));
        }
        if (this.drive.equals(""))
        {
            String messageText = "Must enter a drive.";
            context.addMessage(messageText, new FacesMessage(messageText));
        }
        if (this.diskspace == 0)
        {
            String messageText = "Must enter diskspace.";
            context.addMessage(messageText, new FacesMessage(messageText));
        }
        if (!context.getMessageList().isEmpty())
        {
            return;
        }
        processThread = new ProcessThread(computerName, drive, diskspace, outputThread);
        ProcessExecutor.processThread(processThread);

        refresh();

    }


    public String getOutputInfo() {
        return outputInfo;
    }

    public void setOutputInfo(String outputInfo) {
        this.outputInfo = Output.getOutputString();
    }


    public String getComputerName()
    {
        return computerName;
    }

    public void setComputerName(String computerName)
    {
        this.computerName = computerName;
    }

    public String getDrive()
    {
        return this.drive;
    }

    public void setDrive(String drive)
    {
        this.drive = drive;
    }

    public long getDiskspace()
    {
        return this.diskspace;
    }

    public void setDiskspace(long diskspace) {
        this.diskspace = diskspace;
    }

    public void stopThread()
    {
        processThread.setExit(true);
    }

    public void refresh()
    {
        outputInfo = Output.getOutputString();
    }

}
