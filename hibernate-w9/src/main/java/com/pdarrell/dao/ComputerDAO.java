package com.pdarrell.dao;

import com.pdarrell.Computer;
import com.pdarrell.HibernateUtils;
import com.pdarrell.Status;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

public class ComputerDAO {


    public static Computer getComputer(String computerName)
    {
        try {
            SessionFactory factory = HibernateUtils.getSessionFactory();
            Session session = factory.openSession();

            Query query = session.createQuery("from com.pdarrell.Computer where computerName = '"  + computerName + "'", Computer.class);

            Computer computerFound = (Computer)query.uniqueResult();
            session.close();
            return computerFound;
        } catch (Throwable t)
        {
            System.out.println(t.getMessage());
        } finally {
            //HibernateUtils.shutdown();
        }
        return null;
    }

    public static void storeComputer(Computer computer)
    {
        try {
            SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

            Session session = sessionFactory.openSession();

            Transaction transaction = session.beginTransaction();
            session.save(computer);

            transaction.commit();
            session.close();
        } catch (Throwable t)
        {
            System.out.println(t.getMessage());
        } finally {

            //HibernateUtils.shutdown();

        }
    }


    public static long getMaxId()
    {
        try {
            SessionFactory factory = HibernateUtils.getSessionFactory();
            Session session = factory.openSession();

            Query query = session.createQuery("select max(id) from com.pdarrell.Computer");

            long id = (long)query.uniqueResult();
            session.close();
            return id;
        } catch (Throwable t)
        {
            System.out.println(t.getMessage());
        } finally {
            //HibernateUtils.shutdown();
        }
        return 0;
    }

}
