package com.pdarrell;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class HttpUrl {

    public static void getUrlHeaders(HttpURLConnection urlConnection) {
        Map<String, List<String>> headers = urlConnection.getHeaderFields();
        System.out.println(headers);
        String keyValue = urlConnection.getHeaderField("Location");
        System.out.println(keyValue);

    }

    private static URL getNewUrl(HttpURLConnection urlConnection, URL newURL, String computerName) {
        try {
            newURL = new URL(urlConnection.getURL().toString() +
                    "?jsonComputerStatus=" + Client.sendComputerStatus(
                    Client.createComputerStatus(
                            computerName, "ACTIVE", new Date(), "c:", 8192, false)));
        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return newURL;
    }

    public static HttpURLConnection getHttpURLConnection(HttpURLConnection urlConnection, URL serverURL) {
        try {
            if (serverURL != null) {
                urlConnection = (HttpURLConnection) serverURL.openConnection();
            }
        } catch (IOException ioException) {
            System.out.println(ioException.getMessage());
            ioException.printStackTrace();
        }
        return urlConnection;
    }

}
