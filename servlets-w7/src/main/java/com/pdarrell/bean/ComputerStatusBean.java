package com.pdarrell.bean;

import com.pdarrell.Client;
import com.pdarrell.Server;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import java.util.Date;

public class ComputerStatusBean {
    private String computerName = "";
    private String outputInfo = "";

    public void requestInfo()
    {
        if (this.computerName.equals(""))
        {
            String messageText = "Must enter a computer name.";
            FacesContext.getCurrentInstance().addMessage(messageText, new FacesMessage(messageText));
            return;
        }
        outputInfo = new Server().receiveComputerStatus(Client.sendComputerStatus(
                        Client.createComputerStatus(
                        computerName, "ACTIVE", new Date(), "c:", 8192)));

    }


    public String getOutputInfo() {
        return outputInfo;
    }

    public void setOutputInfo(String outputInfo) {
        this.outputInfo = outputInfo;
    }


    public String getComputerName()
    {
        return computerName;
    }

    public void setComputerName(String computerName)
    {
        this.computerName = computerName;
    }

}
