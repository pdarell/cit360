package com.pdarrell;

import com.fasterxml.jackson.databind.*;
import com.sun.net.httpserver.HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;


public class Server {

    private HttpServer httpServer = null;
    private static ServerSocket socket = null;

    public Server()
    {
        httpServer = createServerHttpServer();
    }

    private HttpServer createServerHttpServer()
    {
        HttpServer httpServer = null;
/*
        try {
            socket = new ServerSocket(8050);
        } catch (IOException e) {
            e.printStackTrace();
        }
*/

        InetSocketAddress socketAddress = new InetSocketAddress("localhost", 8050);
        try {
            httpServer = HttpServer.create(socketAddress, 0);
            httpServer.createContext("/receiveComputerStatus", new ServerHandler());
            ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
            httpServer.setExecutor(threadPoolExecutor);
            httpServer.start();
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

        return httpServer;
    }

    public void connect()
    {
        System.out.println("Connecting to server...");
        processRequest(socket);
    }

    /**
     * Base code by Javarevisited
     * @param socket
     */
    private void processRequest(ServerSocket socket)
    {
        String jsonparam = "";
        while (!socket.isClosed())
        {

            try {

                final Socket request  = socket.accept();
                InputStreamReader streamReader = new InputStreamReader(request.getInputStream());
                if (streamReader != null)
                {
                    BufferedReader bufferreader = new BufferedReader(streamReader);
                    String line = bufferreader.readLine();
                    while (!line.isEmpty())
                    {
                        String param = line.split("//?")[1];
                        String value = param.split("=")[1];
                        if (!value.equals("")) {
                            jsonparam = value.substring(0, (value.indexOf("}") + 1));
                            if (!jsonparam.equals(""))
                            {
                                break;
                            }
                        }
                        line = bufferreader.readLine();
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

            if (!jsonparam.equals(""))
            {

                receiveComputerStatus(jsonparam);

            }

        }
        //stopHttpServer();
    }

    public String receiveComputerStatus(String jsonComputerStatus)
    {
        String error = "";
        ObjectMapper objMapper = new ObjectMapper();
        Status status = null;
        Disk disk = null;
        if (jsonComputerStatus != null && !jsonComputerStatus.equals("")) {
            ComputerStatus computerStatus = null;

            try {
                computerStatus = objMapper.readValue(jsonComputerStatus, ComputerStatus.class);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }

            if (computerStatus != null) {
                status = computerStatus.getStatus();
                disk = computerStatus.getDisk();
            }
        }

        if (status != null) {
            return status.toString();
        } else
        {
            error = ("No status information available.");
        }
        if (disk != null)
        {
            disk.setFreeDiskspaceFormatted(DiskspaceFormatter.calculateDiskSpaceString(disk.getFreeDiskspace()));
            return disk.toString();
        } else
        {
            error = ("No disk information available.");
        }
        if (!error.equals(""))
        {
            return error;
        }
        return null;
    }

    public URL getServerURL()
    {
        URL url = null;
        try {
            if (this.httpServer != null) {
                String protocol = "http";
                String host = this.httpServer.getAddress().getAddress().getHostName();
                int port = this.httpServer.getAddress().getPort();
                String context = "/receiveComputerStatus";
                url = new URL(protocol + "://" + host + ":" + port); //+ context);
            }
        } catch (MalformedURLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return url;
    }

    public void stopHttpServer()
    {
        if (httpServer != null) {
            this.httpServer.stop(0);
            httpServer = null;
        }
        /*if (socket !=  null) {
            try {
                System.out.println("Stopping server...");
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
    }

}
