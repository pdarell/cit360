package com.pdarrell;

import java.net.*;
import java.util.Date;
import java.util.List;
import java.util.Map;


public class Main {

    public static void main(String[] args) {

        Server server = new Server();
        HttpURLConnection urlConnection = null;
        URL newURL = null;
        String computerName = "";
        try {
            computerName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        if (server != null) {

            //server.connect();

            URL serverURL = server.getServerURL();

            urlConnection = HttpUrl.getHttpURLConnection(urlConnection, serverURL);

            if (urlConnection != null) {
                //HttpUrl.getUrlHeaders(urlConnection);

                //newURL = HttpUrl.getNewUrl(urlConnection, newURL, computerName);

                //urlConnection.disconnect();

                //urlConnection = HttpUrl.getHttpURLConnection(urlConnection, newURL);

                urlConnection.disconnect();
            }

            System.out.println("Full computerStatus...");
            server.receiveComputerStatus(Client.sendComputerStatus(
                    Client.createComputerStatus(
                            computerName, "ACTIVE", new Date(), "c:", 8192)));



            System.out.println("No computerStatus...");
            server.receiveComputerStatus(Client.sendComputerStatus(
                    Client.createComputerStatus(
                            computerName, "", null, "", 0)));



            System.out.println("No Status...");
            server.receiveComputerStatus(Client.sendComputerStatus(
                    Client.createComputerStatus(
                            computerName, "", null, "c:", 8192)));



            System.out.println("No Disk...");
            server.receiveComputerStatus(Client.sendComputerStatus(
                    Client.createComputerStatus(
                            computerName, "ACTIVE", new Date(), "", 0)));



            server.stopHttpServer();

        }
        server = null;
    }


}
