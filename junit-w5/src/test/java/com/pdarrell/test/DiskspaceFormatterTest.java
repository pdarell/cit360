package com.pdarrell.test;

import com.pdarrell.DiskspaceFormatter;
import org.hamcrest.Matcher;
import org.hamcrest.MatcherAssert;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DiskspaceFormatterTest {

    @Test
    public void testCalculateBytes()
    {
        double space = 100;
        String formattedDiskspace = DiskspaceFormatter.calculateDiskSpaceString(space);

        MatcherAssert.assertThat(formattedDiskspace, CoreMatchers.containsString( "B"));


    }

    @Test
    public void testCalculateKBytes()
    {
        double space = 1048;
        String formattedDiskspace = DiskspaceFormatter.calculateDiskSpaceString(space);

        MatcherAssert.assertThat(formattedDiskspace, CoreMatchers.containsString("KB"));


    }


    @Test
    public void testCalculateMBytes()
    {
        double space = 10048000;
        String formattedDiskspace = DiskspaceFormatter.calculateDiskSpaceString(space);

        MatcherAssert.assertThat(formattedDiskspace, CoreMatchers.containsString("MB"));


    }


    @Test
    public void testCalculateGBytes()
    {

        double space = 10048000000d;
        String formattedDiskspace = DiskspaceFormatter.calculateDiskSpaceString(space);

        MatcherAssert.assertThat(formattedDiskspace, CoreMatchers.containsString("GB"));

    }

    @Test
    public void testCalculateTBytes()
    {

        double space = 100480000000000d;
        String formattedDiskspace = DiskspaceFormatter.calculateDiskSpaceString(space);

        MatcherAssert.assertThat(formattedDiskspace, CoreMatchers.containsString("TB"));

    }


}
