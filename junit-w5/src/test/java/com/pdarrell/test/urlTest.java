package com.pdarrell.test;

import com.pdarrell.HttpUrl;
import com.pdarrell.Server;
import org.junit.jupiter.api.*;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;

public class urlTest {

    @Test
    public void testCreateConnection()
    {
        Server server = new Server();
        URL serverURL = null;
        HttpURLConnection urlConnection = null;

        try {
            serverURL = server.getServerURL();
            Assertions.assertNotNull(serverURL, "URL is null");

            urlConnection = HttpUrl.getHttpURLConnection(urlConnection, serverURL);

            Assertions.assertNotNull(urlConnection);

            Map<String, List<String>> headers = urlConnection.getHeaderFields();
            Assertions.assertNotNull(headers);
            Assertions.assertNotEquals(true, headers.isEmpty());

            String location = urlConnection.getHeaderField("Location");
            Assertions.assertNull(location, "Location is not null");
        }finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            if (server != null) {
                if (serverURL != null) {
                    server.stopHttpServer();
                    serverURL = null;
                }
                server = null;
            }
        }

    }


}
