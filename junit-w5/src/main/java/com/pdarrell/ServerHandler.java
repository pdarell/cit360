/**
 * Base code by

 CONTRIBUTE ON DZONE
 MVB Program
 Zone Leader Program
 Become a Contributor
 Visit the Writers' Zone

 LEGAL
 Terms of Service
 Privacy Policy


 CONTACT US
 600 Park Offices Drive
 Suite 150
 Research Triangle Park, NC 27709
 support@dzone.com
 +1 (919) 678-0300

 Let's be friends:

 
 
 
 


 DZone.com

 */

package com.pdarrell;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.OutputStream;

public class ServerHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange httpExchange) throws IOException {

            String requestParamValue=null;

            if("GET".equals(httpExchange.getRequestMethod())) {

                requestParamValue = handleGetRequest(httpExchange);

            }else if("POST".equals(httpExchange)) {

                requestParamValue = handlePostRequest(httpExchange);
            }

            handleResponse(httpExchange,requestParamValue);
        }

        private String handleGetRequest(HttpExchange httpExchange) {

            return httpExchange.
            getRequestURI()
                    .toString()
                    .split("\\?")[1]
                    .split("=")[1];
        }

        private void handleResponse(HttpExchange httpExchange, String requestParamValue)  throws  IOException {

            OutputStream outputStream = httpExchange.getResponseBody();
            StringBuilder htmlBuilder = new StringBuilder();

            htmlBuilder.append("<html>").
            append("<body>").
            append("<h1>").
            append("Hello ")
                    .append(requestParamValue)
                    .append("</h1>")
                    .append("</body>")
                    .append("</html>");

            String htmlResponse = htmlBuilder.toString();  //StringEscapeUtils.escapeHtml4(htmlBuilder.toString());

            httpExchange.sendResponseHeaders(200, htmlResponse.length());
            outputStream.write(htmlResponse.getBytes());
            outputStream.flush();
            outputStream.close();
        }

        private String handlePostRequest(HttpExchange httpExchange) {

            return httpExchange.
                    getRequestURI()
                    .toString()
                    .split("/")[1];
        }
    }
