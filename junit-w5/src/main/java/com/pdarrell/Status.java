package com.pdarrell;

import java.util.Date;

public class Status {

    private String computerName = "";
    private Date scanDateTime = null;
    private String statusIndicator = "";


    public String getStatusIndicator() {
        return statusIndicator;
    }

    public void setStatusIndicator(String statusIndicator) {
        this.statusIndicator = statusIndicator;
    }

    public Date getScanDateTime() {
        return scanDateTime;
    }

    public void setScanDateTime(Date scanDateTime) {
        this.scanDateTime = scanDateTime;
    }

    public String getComputerName() {
        return computerName;
    }

    public void setComputerName(String computerName) {
        this.computerName = computerName;
    }

    public String toString()
    {
        return "Status: Computer: " + computerName + ", Indicator: " + statusIndicator + ", Date/Time: " + scanDateTime;
    }
}
