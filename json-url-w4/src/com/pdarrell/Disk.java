package com.pdarrell;

public class Disk {

    private String computerName = "";
    private String drive = "";
    private long freeDiskspace = 0;


    public String getComputerName() {
        return computerName;
    }

    public void setComputerName(String computerName) {
        this.computerName = computerName;
    }

    public String getDrive() {
        return drive;
    }

    public void setDrive(String drive) {
        this.drive = drive;
    }

    public long getFreeDiskspace() {
        return freeDiskspace;
    }

    public void setFreeDiskspace(long freeDiskspace) {
        this.freeDiskspace = freeDiskspace;
    }

    public String toString()
    {
        return "Disk: Computer: " + computerName + ", Drive: " + drive + ", free diskspace: " + freeDiskspace;
    }
}
