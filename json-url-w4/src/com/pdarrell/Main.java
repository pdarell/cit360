package com.pdarrell;

import javax.net.ssl.HttpsURLConnection;
import java.io.IOException;
import java.net.*;
import java.net.InetAddress.*;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {

        Server server = new Server();
        HttpURLConnection urlConnection = null;
        URL newURL = null;
        String computerName = "";
        try {
            computerName = InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        if (server != null) {

            //server.connect();

            //createServerThread(server);
/*
            URL serverURL = server.getServerURL();

            try {
                if (serverURL != null) {
                    urlConnection = (HttpURLConnection) serverURL.openConnection();
                }
            } catch (IOException ioException) {
                System.out.println(ioException.getMessage());
                ioException.printStackTrace();
            }
            if (urlConnection != null) {
                /*Map<String, List<String>> headers = urlConnection.getHeaderFields();
                System.out.println(headers);
                String keyValue = urlConnection.getHeaderField("Location");
                System.out.println(keyValue);


                try {
                    newURL = new URL(urlConnection.getURL().toString() +
                            "?jsonComputerStatus=" + Client.sendComputerStatus(
                            Client.createComputerStatus(
                                    computerName, "ACTIVE", new Date(), "c:", 8192)));
                } catch (MalformedURLException e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
                urlConnection.disconnect();
                try {
                    urlConnection = (HttpURLConnection) newURL.openConnection();
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
            }
 */
            System.out.println("Full computerStatus...");
            server.receiveComputerStatus(Client.sendComputerStatus(
                    Client.createComputerStatus(
                            computerName, "ACTIVE", new Date(), "c:", 8192)));



            System.out.println("No computerStatus...");
            server.receiveComputerStatus(Client.sendComputerStatus(
                    Client.createComputerStatus(
                            computerName, "", null, "", 0)));



            System.out.println("No Status...");
            server.receiveComputerStatus(Client.sendComputerStatus(
                    Client.createComputerStatus(
                            computerName, "", null, "c:", 8192)));



            System.out.println("No Disk...");
            server.receiveComputerStatus(Client.sendComputerStatus(
                    Client.createComputerStatus(
                            computerName, "ACTIVE", new Date(), "", 0)));

        }

        server.stopHttpServer();
    }

    private static void createServerThread(Server server)
    {
        server.start();
    }


}
