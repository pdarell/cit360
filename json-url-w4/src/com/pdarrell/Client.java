package com.pdarrell;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Date;

public class Client {

    public static String sendComputerStatus(ComputerStatus computerStatus)
    {
        ObjectMapper objMapper = new ObjectMapper();
        String returnValue = "";
        try {
            returnValue = objMapper.writeValueAsString(computerStatus);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return returnValue;
    }

    public static ComputerStatus createComputerStatus(String computerName, String statusInd, Date datetime, String drive, long space)
    {
        Status status = null;
        Disk disk = null;
        if (!computerName.equals("") && !statusInd.equals("") && datetime != null) {
            status = createStatus(computerName, statusInd, datetime);
        }
        if (!computerName.equals("") && !drive.equals("")) {
            disk = createDisk(computerName, drive, space);
        }
        ComputerStatus computerStatus = new ComputerStatus();
        computerStatus.setStatus(status);
        computerStatus.setDisk(disk);

        return computerStatus;

    }

    private static Status createStatus(String computerName, String statusInd, Date datetime)
    {
        Status status = new Status();
        status.setComputerName(computerName);
        status.setStatusIndicator(statusInd);
        status.setScanDateTime(datetime);
        return status;
    }

    private static Disk createDisk(String computerName, String drive, long space)
    {
        Disk disk = new Disk();
        disk.setComputerName(computerName);
        disk.setDrive(drive);
        disk.setFreeDiskspace(space);
        return disk;
    }



}
