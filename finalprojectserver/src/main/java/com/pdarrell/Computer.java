package com.pdarrell;

import com.pdarrell.dao.ComputerDAO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;


public class Computer {


    private long computerId = 0;


    private String computerName = "";


    private boolean inActive;


    public long getComputerId() {
        return computerId;
    }

    public void setComputerId(long computerId) {
        this.computerId = computerId;
    }

    public String getComputerName() {
        return computerName;
    }

    public void setComputerName(String computerName) {
        this.computerName = computerName;
    }

    public boolean isInActive() {
        return inActive;
    }

    public void setInActive(boolean inActive) {
        this.inActive = inActive;
    }

    private long getMaxId()
    {
        return ComputerDAO.getMaxId();
    }

}
