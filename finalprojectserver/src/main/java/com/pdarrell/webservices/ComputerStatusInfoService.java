package com.pdarrell.webservices;

import com.pdarrell.Server;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;


@Path("/computerstatusinfo")
public class ComputerStatusInfoService {

    @POST
    @Path("/computerstatus")
    @Consumes(MediaType.APPLICATION_JSON)
    public void receiveComputerStatusInfo(String computerStatusJSON)
    {

        if (!computerStatusJSON.equals(""))
        {
            Server.receiveComputerStatus(computerStatusJSON);
        }

    }


}
