package com.pdarrell;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;


public class ParseDate {

	/** Parse the date into date segments
	 * 
	 * @param dbDateString
	 * @return
	 */
	public Date parseDate(String dbDateString)
	{
		String lastChars = dbDateString.substring((dbDateString.length()-2), dbDateString.length());
		if (lastChars.equalsIgnoreCase("PM") ||lastChars.equalsIgnoreCase("AM"))
		{ 
			SimpleDateFormat dateFormat = null;
			dateFormat = new SimpleDateFormat("MMM dd yyyy hh:mmaaa");
			dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
			try {
				return dateFormat.parse(dbDateString);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String dateString = "";
 		String retValue = "";
 		String[] pmValues = new String[] {"12"};
 		List<String> pmArrayList = Arrays.asList(pmValues);
 		boolean pm = false;
		int dotCount = 0;
		for (int i= 0; i< dbDateString.length(); i++){
			if (dbDateString.charAt(i) == '.')
			{
				switch (dotCount)
				{
					case 0: //year
						dateString = retValue;
						retValue = "";
						break;
					case 1: //month
						dateString += "-" + retValue;
						retValue = "";
						break;
					case 2: //day
						dateString += "-" + retValue;
						retValue = "";
						break;
					case 3: //hour
						dateString += "T" + retValue;
						if (pmArrayList.contains(retValue))
						{
							pm = true;
						}else
						{
							pm = false;
						}
						retValue = "";
						break;
					case 4: //minute
						dateString += ":" + retValue;
						retValue = "";
						break;
					case 5: //seconds
						dateString += ":" + retValue;
						retValue = "";
						break;					
					default:
						dateString += ":" + retValue;	
				}
				dotCount++;
			}else
			{
				retValue += dbDateString.charAt(i);
			}
		}
		dateString += ":" + retValue;
		
		
		if (pm) { dateString += " " + "PM"; } //else { dateString += " " + "AM"; }
		 
		
		Date dateValue = new Date();		
		try {
			SimpleDateFormat dateFormat = null;
			if (pm)
			{
				dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss aaa");
			}else
			{
				dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");
			}
			dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
			dateValue = dateFormat.parse(dateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return dateValue;
	}
	
	/** Check if the time is within an hour
	 * 
	 * @param dbDate
	 * @return
	 */
	public boolean isWithinHour(Date dbDate)
	{
		
		Date d1 = dbDate;
		Date d2 = new Date();
		
		// Get msec from each, and subtract.
		long diff = d2.getTime() - d1.getTime();
		//long diffSeconds = diff / 1000;         
		long diffMinutes = diff / (60 * 1000);         
		long diffHours = diff / (60 * 60 * 1000);                      
		//System.out.println("Time in seconds: " + diffSeconds + " seconds.");         
		//System.out.println("Time in minutes: " + diffMinutes + " minutes.");         
		//System.out.println("Time in hours: " + diffHours + " hours."); 
		// change minutes to 60 from 59, give full hour in minutes.
		if (diffHours > 1 || diffMinutes > 60) {
			return false;
		}
		
		return true;
	}
	
	
	public String formatDateTime(Date dbDate)
	{
		SimpleDateFormat dateFormat = new SimpleDateFormat("MMMMM dd, yyyy hh:mm:ss aaa");
		dateFormat.setTimeZone(TimeZone.getTimeZone("America/New_York"));
		return dateFormat.format(dbDate);
		
	}
	
}
