package com.pdarrell;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class WebservicesClient {


    public static List<ComputerStatus> getComputerStatuses(long offset)
    {
        List<ComputerStatus> computerStatuses = new ArrayList<ComputerStatus>();
        Client client = new Client();

        String pageOffsetjson = "";
        ObjectMapper mapper = new ObjectMapper();
        PageOffset pageOffset = new PageOffset();
        try {
            pageOffset.setOffset(offset);
            pageOffsetjson = mapper.writeValueAsString(pageOffset);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        WebResource webResource = client.resource("http://localhost:8081/finalprojectws_war_exploded/" + "monitor" +"/" + "computerstatuses");

        ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON).post(ClientResponse.class, pageOffsetjson);

        if (response != null)
        {

            if (response.getStatus() != 200) {

                System.out.println("Failed : HTTP error code : "
                        + response.getStatus() + " - " + response.getStatusInfo() + " : " + webResource.toString());
            } else
            {
                String json = response.getEntity(String.class);
                processJSONToComputerStatuses(computerStatuses, json);
            }

        }
        return computerStatuses;
    }

    public static List<ComputerStatus> getLatestComputerStatuses()
    {
        List<ComputerStatus> computerStatuses = new ArrayList<ComputerStatus>();
        Client client = new Client();

        WebResource webResource = client.resource("http://localhost:8081/finalprojectws_war_exploded/" + "monitor" +"/" + "latestcomputerstatuses");

        ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        if (response != null)
        {

            if (response.getStatus() != 200) {

                System.out.println("Failed : HTTP error code : "
                        + response.getStatus() + " - " + response.getStatusInfo() + " : " + webResource.toString());
            } else
            {
                String json = response.getEntity(String.class);
                processJSONToComputerStatuses(computerStatuses, json);
            }

        }
        return computerStatuses;
    }

    private static void processJSONToComputerStatuses(List<ComputerStatus> computerStatuses, String json) {
        if (json != null)
        {
            System.out.println(json);
            ObjectMapper objMapper = new ObjectMapper();

            for (int i = 0; i < json.length(); i++)
            {
                int index = json.indexOf("ComputerStatus", i);
                int end = json.indexOf("ComputerStatus", (index + 13));

                try {
                    String computerStatus = "";
                    if (end == -1)
                    {
                        computerStatus = json.substring((index - 1), json.length());
                    } else {
                        computerStatus = json.substring((index - 1), (end - 4));
                    }

                    int statusindex = computerStatus.indexOf("{\"Status");
                    int statusend = computerStatus.indexOf( "{\"Disk",(statusindex));
                    String statusjson = computerStatus.substring(statusindex+10, statusend-2);


                    int diskindex = computerStatus.indexOf("{\"Disk");
                    int diskend = computerStatus.indexOf( "},",(diskindex));
                    String diskjson = computerStatus.substring(diskindex+8, computerStatus.length());

                    Status status = objMapper.readValue(statusjson, Status.class);
                    Disk disk = objMapper.readValue(diskjson, Disk.class);

                    ComputerStatus computerStatusObj = new ComputerStatus();
                    computerStatusObj.setStatus(status);
                    computerStatusObj.setDisk(disk);
                    computerStatuses.add(computerStatusObj);

                    //computerstatuses = objMapper.readValue(json, List.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (end == -1)
                {
                   i = json.length();
                } else {
                    i = (end - 1);
                }
            }
        }
    }

    public static List<Computer> getComputers()
    {
        List<Computer> computers = new ArrayList<Computer>();
        Client client = new Client();

        WebResource webResource = client.resource("http://localhost:8081/finalprojectws_war_exploded/" + "monitor" +"/" + "computers");

        ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        if (response != null)
        {

            if (response.getStatus() != 200) {

                System.out.println("Failed : HTTP error code : "
                        + response.getStatus() + " - " + response.getStatusInfo() + " : " + webResource.toString());
            } else
            {
                String json = response.getEntity(String.class);
                processJSONToComputers(computers, json);
            }

        }
        return computers;
    }

    private static void processJSONToComputers(List<Computer> computers, String json)
    {
        if (!json.equals(""))
        {
            System.out.println(json);
            ObjectMapper objMapper = new ObjectMapper();

            try {
                for (int i=0; i < json.length(); i++) {
                    int index = json.indexOf("{\"computerId", i);
                    int end = json.indexOf("},", index);
                    String computerjson = "";
                    if (end == -1) {
                        computerjson = json.substring(index, json.length()-1);
                    } else {
                        computerjson = json.substring(index, end+1);
                    }

                    Computer computer = objMapper.readValue(computerjson, Computer.class);

                    computers.add(computer);
                    if (end == -1) {
                       i = json.length();
                    } else {
                        i = (end + 1);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void inactiveComputer(Computer computer)
    {
        if (computer != null) {
            Client client = new Client();

            ObjectMapper mapper = new ObjectMapper();

            String json = null;
            try {
                json = mapper.writeValueAsString(computer);
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            WebResource webResource = client.resource("http://localhost:8081/finalprojectws_war_exploded/" + "monitor" + "/" + "inactivatecomputer");

            ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON)
                    .type(MediaType.APPLICATION_JSON).post(ClientResponse.class, json);

            if (response != null) {

                if (response.getStatus() != 200) {

                    System.out.println("Failed : HTTP error code : "
                            + response.getStatus() + " - " + response.getStatusInfo() + " : " + webResource.toString());
                }
            }
        }
    }


    public static BigInteger getNumberRows()
    {
        Client client = new Client();

        WebResource webResource = client.resource("http://localhost:8081/finalprojectws_war_exploded/" + "monitor" +"/" + "numberrows");

        ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON).get(ClientResponse.class);

        if (response != null)
        {

            if (response.getStatus() != 200) {

                System.out.println("Failed : HTTP error code : "
                        + response.getStatus() + " - " + response.getStatusInfo() + " : " + webResource.toString());
            } else
            {
                String json = response.getEntity(String.class);
                BigInteger numrows = processJSONToCount(json);
                return numrows;
            }

        }
        return null;
    }

    private static BigInteger processJSONToCount(String json){

        if (json != null && !json.equals(""))
        {
            ObjectMapper mapper = new ObjectMapper();
            try {
                Count count  = mapper.readValue(json, Count.class);
                if (count != null){
                    BigInteger numrows = count.getCount();
                    return numrows;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }


}
