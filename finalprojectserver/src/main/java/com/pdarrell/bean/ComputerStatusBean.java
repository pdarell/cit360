package com.pdarrell.bean;


import com.pdarrell.Computer;
import com.pdarrell.ComputerStatus;
import com.pdarrell.WebservicesClient;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public class ComputerStatusBean {
    private String computerName = "";
    private String outputInfo = "";
    private String drive = "";
    private long diskspace = 0;
    private List<ComputerStatus> computerStatuses = null;
    private boolean enableInfo = true;
    private boolean enableComputers = false;
    private List<Computer> computers = null;
    private BigInteger numberRows;
    private int page = 1;
    private Double pages;
    private boolean renderPrevious = false;
    private boolean renderNext = false;



    public void requestInfo() {
        if (numberRows == null)
        {
            numberRows = WebservicesClient.getNumberRows();
        }
        long offset = calculatePageOffset();
        setRenderNext(pages > 1 && page != pages);
        setRenderPrevious(page != 1);
        computerStatuses = WebservicesClient.getComputerStatuses(offset);
        enableInfo = true;
        enableComputers = false;
    }


    public String getOutputInfo() {
        return outputInfo;
    }

    public String getComputerName()
    {
        return computerName;
    }

    public void setComputerName(String computerName)
    {
        this.computerName = computerName;
    }

    public String getDrive()
    {
        return this.drive;
    }

    public void setDrive(String drive)
    {
        this.drive = drive;
    }

    public long getDiskspace()
    {
        return this.diskspace;
    }

    public void setDiskspace(long diskspace) {
        this.diskspace = diskspace;
    }

    public void refresh()
    {
        computerStatuses = WebservicesClient.getLatestComputerStatuses();
        enableInfo = true;
        enableComputers = false;
        renderNext = false;
        renderPrevious = false;
    }

    public List<ComputerStatus> computerStatusInfo()
    {
        return computerStatuses;
    }

    public boolean isEnableInfo() {
        return enableInfo;
    }

    public void setEnableInfo(boolean enableInfo) {
        this.enableInfo = enableInfo;
    }

    public boolean isEnableComputers() {
        return enableComputers;
    }

    public void setEnableComputers(boolean enableComputers) {
        this.enableComputers = enableComputers;
    }

    public List<Computer> computersInfo()
    {
        return computers;
    }

    public void retrieveComputerInfo()
    {
        computers = WebservicesClient.getComputers();
        enableInfo = false;
        enableComputers = true;
        renderNext = false;
        renderPrevious = false;
    }

    public void inActivateComputer(Computer computer)
    {
        if (computer != null)
        {
            computer.setInActive(true);
        }
        WebservicesClient.inactiveComputer(computer);
        retrieveComputerInfo();
    }

    public void activateComputer(Computer computer)
    {
        if (computer != null)
        {
            computer.setInActive(false);
        }
        WebservicesClient.inactiveComputer(computer);
        retrieveComputerInfo();

    }

    private long calculatePageOffset()
    {
        long offset = 0;

        pages = Math.ceil(numberRows.doubleValue()/25);

        if (page > pages)
        {
            page = 1;
        }

        offset = (page -1) * 25;

        return offset;
    }

    public void nextRecords()
    {
        page++;

        requestInfo();
    }


    public void previousRecords()
    {
        page--;
        if (page < 1)
        {
            page = 1;
        }

        requestInfo();
    }

    public void firstRecords()
    {
        page = 1;

        requestInfo();
    }

    public void lastRecords()
    {
        page = (pages.intValue() -1);

        requestInfo();
    }

    public boolean isRenderPrevious() {
        return renderPrevious;
    }

    public void setRenderPrevious(boolean renderPrevious) {
        this.renderPrevious = renderPrevious;
    }

    public boolean isRenderNext() {
        return renderNext;
    }

    public void setRenderNext(boolean renderNext) {
        this.renderNext = renderNext;
    }
}
