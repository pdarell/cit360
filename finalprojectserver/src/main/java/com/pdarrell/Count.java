package com.pdarrell;

import java.math.BigInteger;

public class Count {

    private BigInteger count;

    public BigInteger getCount() {
        return count;
    }

    public void setCount(BigInteger count) {
        this.count = count;
    }
}
