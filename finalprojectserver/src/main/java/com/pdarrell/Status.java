package com.pdarrell;

import com.pdarrell.dao.StatusDAO;

import javax.persistence.*;
import java.util.Date;


public class Status {



    private Computer computer = null;

    private long id = 0;

    private Date scanDateTime = null;


    private String statusIndicator = "";


    public String getStatusIndicator() {
        return statusIndicator;
    }

    public void setStatusIndicator(String statusIndicator) {
        this.statusIndicator = statusIndicator;
    }

    public Date getScanDateTime() {
        return scanDateTime;
    }

    public void setScanDateTime(Date scanDateTime) {
        this.scanDateTime = scanDateTime;
    }

    public Computer getComputer() {
        return computer;
    }

    public void setComputer(Computer computer) {
        this.computer = computer;
    }

    public String toString()
    {
        return "Status: Computer: " + computer.getComputerName() + ", Indicator: " + statusIndicator + ", Date/Time: " + scanDateTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
