package com.pdarrell.dao;

import com.pdarrell.Computer;
import com.pdarrell.HibernateUtils;
import com.pdarrell.Status;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;


public class StatusDAO {


    public static void storeStatus(Status status)
    {
        try {
            SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

            Session session = sessionFactory.openSession();

            Transaction transaction = session.beginTransaction();
            Computer computerFound = ComputerDAO.getComputer(status.getComputer().getComputerName());
            if (computerFound != null)
            {
                status.setComputer(computerFound);
            } else
            {
                ComputerDAO.storeComputer(status.getComputer());
            }
            session.save(status);

            transaction.commit();
            session.close();
        } catch (Throwable t)
        {
            System.out.println(t.getMessage());
        } finally {

            //HibernateUtils.shutdown();

        }
    }


    public static List<Status> readStatuses()
    {
        List<Status> statuses = null;
        try {
            SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

            Session session = sessionFactory.openSession();
            Query query = session.createQuery("from com.pdarrell.Status", Status.class);
            statuses = query.list();
            session.close();

        } catch (Throwable t)
        {
            System.out.println(t.getMessage());
        } finally {

            //HibernateUtils.shutdown();
        }
        return statuses;
    }

    public static long getMaxId()
    {
        try {
            SessionFactory factory = HibernateUtils.getSessionFactory();
            Session session = factory.openSession();

            Query query = session.createQuery("select max(id) from com.pdarrell.Status");

            long id = (long)query.uniqueResult();
            session.close();
            return id;
        } catch (Throwable t)
        {
            System.out.println(t.getMessage());
        } finally {
            //HibernateUtils.shutdown();
        }
        return 0;
    }

}
