package com.pdarrell.dao;

import com.pdarrell.Computer;
import com.pdarrell.Disk;
import com.pdarrell.HibernateUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class DiskDAO {


    public static void storeDisk(Disk disk)
    {
        try {
            SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

            Session session = sessionFactory.openSession();

            Transaction transaction = session.beginTransaction();
            Computer computerFound = ComputerDAO.getComputer(disk.getComputer().getComputerName());
            if (computerFound != null)
            {
                disk.setComputer(computerFound);
            } else
            {
                ComputerDAO.storeComputer(disk.getComputer());
            }
            session.save(disk);

            transaction.commit();
            session.close();
        } catch (Throwable t)
        {
            System.out.println(t.getMessage());
        } finally {

            //HibernateUtils.shutdown();

        }
    }


    public static List<Disk> readDisks()
    {
        List<Disk> disks = null;
        try {
            SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

            Session session = sessionFactory.openSession();
            Query query = session.createQuery("from com.pdarrell.Disk", Disk.class);
            disks = query.list();
            session.close();

        } catch (Throwable t)
        {
            System.out.println(t.getMessage());
        } finally {

            //HibernateUtils.shutdown();
        }
        return disks;
    }

    public static long getMaxId()
    {
        try {
            SessionFactory factory = HibernateUtils.getSessionFactory();
            Session session = factory.openSession();

            Query query = session.createQuery("select max(id) from com.pdarrell.Disk");

            long id = (long)query.uniqueResult();
            session.close();
            return id;
        } catch (Throwable t)
        {
            System.out.println(t.getMessage());
        } finally {
            //HibernateUtils.shutdown();
        }
        return 0;
    }

}
