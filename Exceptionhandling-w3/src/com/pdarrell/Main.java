package com.pdarrell;

public class Main {

    public static void main(String[] args) {
        //prompt for numbers and validate

        int firstNumber = 0;
        int secondNumber = 0;
        int result = 0;
        // exception handling
        System.out.println("Exception handling");
        try {
            firstNumber = Inputs.promptFirstNumber("Enter first number: ");

            secondNumber = Inputs.promptSecondNumber("Enter second number: ");

            result = calculatedivide(firstNumber, secondNumber);

        } catch (ArithmeticException ex)
        {
            System.out.println(ex.getMessage());
            result = promptSecondCalculate(firstNumber, "Reenter the second number, not a zero (0): ");
        } finally {

            System.out.println("Result is : " + result);
        }

        result = 0;
        result = validationCalculation();

        System.out.println("Result is : " +result);

    }

    private static int promptSecondCalculate(int firstNumber, String s) {
        int secondNumber;
        secondNumber = Inputs.promptSecondNumber(s);
        int result = 0;
        try {
            result = calculatedivide(firstNumber, secondNumber);
        } catch (ArithmeticException ex)
        {
            System.out.println(ex.getMessage());
            result = promptSecondCalculate(firstNumber, s);
        }
        return result;
    }

    private static int validationCalculation() {
        int firstNumber = 0;
        int result = 0;
        int secondNumber = 0;
        // validation
        System.out.println("Validation");
        firstNumber = 0;
        secondNumber = 0;
        firstNumber = Inputs.promptFirstNumber("Enter first number: ");

        secondNumber = Inputs.promptSecondNumber("Enter second number: ");
        String errorMsg = Validation.validateSecondNumber(secondNumber);
        if (errorMsg != null)
        {
            System.out.println(errorMsg);
            result = validationCalculation();
        } else {
            result = calculatedivide(firstNumber, secondNumber);
        }
        return result;
    }


    private static int calculatedivide(int firstNumber, int secondNumber) throws ArithmeticException
    {
        int result;
        try {
            result = firstNumber / secondNumber;
        } catch (Exception ex) {
            throw new ArithmeticException(ex.getMessage());
        }
        return result;
    }


}
