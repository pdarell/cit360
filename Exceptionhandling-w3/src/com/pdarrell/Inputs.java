package com.pdarrell;

import java.util.Scanner;

public class Inputs {



    public static int promptFirstNumber(String msg)
    {
        Scanner input = new Scanner(System.in);

        //prompt "Enter first number: "
        System.out.print(msg);

        return input.nextInt();
    }

    public static int promptSecondNumber(String msg)
    {
        Scanner input = new Scanner(System.in);

        //prompt "Enter second number: "
        System.out.print(msg);

        return input.nextInt();

    }


}
