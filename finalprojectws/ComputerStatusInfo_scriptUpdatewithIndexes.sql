-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema ComputerStatusInfo
-- -----------------------------------------------------
-- ComputerStatusInfo database for CIT 360 OOD class
DROP SCHEMA IF EXISTS `ComputerStatusInfo` ;

-- -----------------------------------------------------
-- Schema ComputerStatusInfo
--
-- ComputerStatusInfo database for CIT 360 OOD class
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ComputerStatusInfo` DEFAULT CHARACTER SET utf8 ;
USE `ComputerStatusInfo` ;

-- -----------------------------------------------------
-- Table `ComputerStatusInfo`.`Computer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ComputerStatusInfo`.`Computer` ;

CREATE TABLE IF NOT EXISTS `ComputerStatusInfo`.`Computer` (
  `Computer_id` DECIMAL(18,0) NOT NULL,
  `ComputerName` VARCHAR(50) NOT NULL,
  `InActive` INT(1) NOT NULL,
  PRIMARY KEY (`Computer_id`),
  UNIQUE INDEX `Computer_pk_UNIQUE` (`Computer_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ComputerStatusInfo`.`Status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ComputerStatusInfo`.`Status` ;

CREATE TABLE IF NOT EXISTS `ComputerStatusInfo`.`Status` (
  `Status_id` DECIMAL(18,0) NOT NULL,
  `Status_Computer_id` DECIMAL(18,0) NOT NULL,
  `ScanDateTime` DATETIME NOT NULL,
  `StatusInd` VARCHAR(10) NOT NULL,
  UNIQUE INDEX `Status_id_UNIQUE` (`Status_id` ASC),
  PRIMARY KEY (`Status_id`),
  INDEX `ComputerStatus_Computer_id_idx` (`Status_Computer_id` ASC),
  CONSTRAINT `fkComputerStatus_Computer_id`
    FOREIGN KEY (`Status_Computer_id`)
    REFERENCES `ComputerStatusInfo`.`Computer` (`Computer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ComputerStatusInfo`.`Disk`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ComputerStatusInfo`.`Disk` ;

CREATE TABLE IF NOT EXISTS `ComputerStatusInfo`.`Disk` (
  `Disk_id` DECIMAL(18,0) NOT NULL,
  `Disk_Computer_id` DECIMAL(18,0) NOT NULL,
  `Drive` VARCHAR(50) NOT NULL,
  `ScanDateTime` DATETIME NOT NULL,
  `TotalDiskspace` VARCHAR(50) NULL,
  `FreeDiskspace` DECIMAL(18,2) NOT NULL,
  PRIMARY KEY (`Disk_id`),
  UNIQUE INDEX `Disk_id_UNIQUE` (`Disk_id` ASC),
  INDEX `fvDisk_Computer_id_idx` (`Disk_Computer_id` ASC),
  CONSTRAINT `fvDisk_Computer_id`
    FOREIGN KEY (`Disk_Computer_id`)
    REFERENCES `ComputerStatusInfo`.`Computer` (`Computer_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

create index idxStatusScanDateTime on ComputerStatusInfo.Status (ScanDateTime);
create index idxDiskScanDateTime on ComputerStatusInfo.Disk (ScanDateTime);
create index idxComputerComputerName on ComputerStatusInfo.Computer (ComputerName);
create index idxComputerComputerId on ComputerStatusInfo.Computer (Computer_id);
create index idxStatusComputerId on ComputerStatusInfo.Status (Status_Computer_id);
create index idxDiskComputerId on ComputerStatusInfo.Disk (Disk_Computer_id);
create index idxStatusComputerIdScan on ComputerStatusInfo.Status (Status_Computer_id, ScanDateTime);
create index idxDiskComputerIdScan on ComputerStatusInfo.Disk (Disk_Computer_id, ScanDateTime);
create index idxDiskDrive on ComputerStatusInfo.Disk (Drive);


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
