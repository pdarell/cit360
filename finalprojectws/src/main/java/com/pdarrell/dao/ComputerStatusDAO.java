package com.pdarrell.dao;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pdarrell.*;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

public class ComputerStatusDAO {


    public static String getLatestComputerStatuses()
    {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

        Session session = sessionFactory.openSession();

        SQLQuery query = session.createSQLQuery("select Computer_id, ComputerName, InActive, StatusInd, max(cs.ScanDateTime), Drive, FreeDiskspace, TotalDiskspace, max(d.ScanDateTime)" +
                " from computerstatusinfo.Computer join computerstatusinfo.status as cs on Computer_id = Status_Computer_id" +
                " join computerstatusinfo.disk as d on Computer_id = Disk_Computer_id" +
                " group by ComputerName, Drive");

        List<Object[]> statuses = query.list();

        String json = "";

        json = createJSON(statuses, json);

        session.close();
        return json;
    }

    private static String createJSON(List<Object[]> statuses, String json) {
        if (statuses != null) {
            ObjectMapper objMapper = new ObjectMapper();

            int i = 0;
            for (Object[] obj : statuses) {
                try {
                    Computer computer = new Computer();
                    computer.setComputerId(Long.parseLong(obj[0].toString()));
                    computer.setComputerName(obj[1].toString());
                    boolean inactive = false;
                    int intactive = Integer.parseInt(obj[2].toString());
                    if (intactive == 1) {
                        inactive = true;
                    }
                    computer.setInActive(inactive);

                    Status status = new Status();
                    status.setComputer(computer);
                    status.setStatusIndicator(obj[3].toString());
                    status.setScanDateTime(ParseDate.parseDate(obj[4].toString()));

                    Disk disk = new Disk();
                    disk.setComputer(computer);
                    disk.setDrive(obj[5].toString());
                    disk.setFreeDiskspace(Double.parseDouble(obj[6].toString()));
                    disk.setFreeDiskspaceFormatted(obj[7].toString());
                    disk.setScanDateTime(ParseDate.parseDate(obj[8].toString()));

                    i++;
                    if (json.equals(""))
                    {
                        json += "[";
                        json += "{\"ComputerStatus\" : {\"Status\":" + objMapper.writeValueAsString(status);
                        json += ", {\"Disk\":" + objMapper.writeValueAsString(disk);
                    } else
                    {
                        json += ", {\"ComputerStatus\" : {\"Status\":" + objMapper.writeValueAsString(status);
                        json += ", {\"Disk\":" + objMapper.writeValueAsString(disk);
                        if (i == statuses.size())
                        {
                            json += "]";
                        }
                    }

                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        }
        return json;
    }


    public static String getComputerStatuses(long offset)
    {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

        Session session = sessionFactory.openSession();

        SQLQuery query = session.createSQLQuery("select Computer_id, ComputerName, InActive, StatusInd, cs.ScanDateTime statusScanDate, Drive, FreeDiskspace, TotalDiskspace, d.ScanDateTime diskScanDate" +
                " from computerstatusinfo.Computer join computerstatusinfo.status as cs on Computer_id = Status_Computer_id" +
                " join computerstatusinfo.disk as d on Computer_id = Disk_Computer_id" +
                " LIMIT " + offset + "," + 25);
                //" order by statusScanDate desc, ComputerName asc, Drive asc");

        List<Object[]> statuses = query.list();

        String json = "";

        json = createJSON(statuses, json);

        session.close();
        return json;


    }


    public static String getNumberRows()
    {
        SessionFactory sessionFactory = HibernateUtils.getSessionFactory();

        Session session = sessionFactory.openSession();

        SQLQuery query = session.createSQLQuery("select Count(*)" +
                " from computerstatusinfo.Computer join computerstatusinfo.status as cs on Computer_id = Status_Computer_id" +
                " join computerstatusinfo.disk as d on Computer_id = Disk_Computer_id");

        BigInteger count = (BigInteger) query.uniqueResult();

        session.close();

        return createCountJSON(count);
    }

    private static String createCountJSON(BigInteger count)
    {
        return "{ \"count\": " + count + "}";
    }

}
