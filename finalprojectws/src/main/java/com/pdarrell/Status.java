package com.pdarrell;

import com.pdarrell.dao.StatusDAO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="Status", schema = "ComputerStatusInfo")
public class Status {


    @JoinColumn(name="Status_Computer_id", nullable = false)
    @ManyToOne(targetEntity = Computer.class, fetch = FetchType.EAGER)
    private Computer computer = null;

    @Id
    @Column(name = "Status_id")
    private long id = getMaxId() + 1;

    @Column(name = "ScanDateTime")
    private Date scanDateTime = null;

    @Column(name = "StatusInd")
    private String statusIndicator = "";


    public String getStatusIndicator() {
        return statusIndicator;
    }

    public void setStatusIndicator(String statusIndicator) {
        this.statusIndicator = statusIndicator;
    }

    public Date getScanDateTime() {
        return scanDateTime;
    }

    public void setScanDateTime(Date scanDateTime) {
        this.scanDateTime = scanDateTime;
    }

    public Computer getComputer() {
        return computer;
    }

    public void setComputer(Computer computer) {
        this.computer = computer;
    }

    public String toString()
    {
        return "Status: Computer: " + computer.getComputerName() + ", Indicator: " + statusIndicator + ", Date/Time: " + scanDateTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    private long getMaxId()
    {
        return StatusDAO.getMaxId();
    }


}
