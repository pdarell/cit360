package com.pdarrell;

import com.pdarrell.dao.DiskDAO;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="Disk", schema = "ComputerStatusInfo")
public class Disk {

    @JoinColumn(name="Disk_Computer_id", nullable = false)
    @ManyToOne(targetEntity = Computer.class, fetch = FetchType.EAGER)
    private Computer computer = null;

    @Id
    @Column(name = "Disk_id")
    private long id = getMaxId() + 1;

    @Column(name = "Drive")
    private String drive = "";

    @Column(name = "FreeDiskspace")
    private double freeDiskspace = 0;

    @Column(name = "TotalDiskspace")
    private String freeDiskspaceFormatted = "";

    @Column(name = "ScanDateTime")
    private Date scanDateTime = null;


    public Computer getComputer() {
        return computer;
    }

    public void setComputer(Computer computer) {
        this.computer = computer;
    }

    public String getDrive() {
        return drive;
    }

    public void setDrive(String drive) {
        this.drive = drive;
    }

    public double getFreeDiskspace() {
        return freeDiskspace;
    }

    public void setFreeDiskspace(double freeDiskspace) {
        this.freeDiskspace = freeDiskspace;
    }

    public String toString()
    {
        return "Disk: Computer: " + computer.getComputerName() + ", Drive: " + drive + ", free diskspace: " + freeDiskspaceFormatted;
    }

    public String getFreeDiskspaceFormatted() {
        return freeDiskspaceFormatted;
    }

    public void setFreeDiskspaceFormatted(String freeDiskspaceFormatted) {
        this.freeDiskspaceFormatted = freeDiskspaceFormatted;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    private long getMaxId()
    {
        return DiskDAO.getMaxId();
    }

    public Date getScanDateTime() {
        return scanDateTime;
    }

    public void setScanDateTime(Date scanDateTime) {
        this.scanDateTime = scanDateTime;
    }
}
