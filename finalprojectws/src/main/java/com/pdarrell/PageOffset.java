package com.pdarrell;

public class PageOffset {

    private long offset = 0;


    public long getOffset() {
        return offset;
    }

    public void setOffset(long offset) {
        this.offset = offset;
    }
}
