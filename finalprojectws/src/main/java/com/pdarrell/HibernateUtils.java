
package com.pdarrell;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class HibernateUtils {


    private static SessionFactory sessionFactory = createSessionFactory();

    private static SessionFactory createSessionFactory()
    {

            //StandardServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder().configure("/hibernate.cfg.xml").build();

            //Metadata metadata = new MetadataSources(serviceRegistry).getMetadataBuilder().build();

            //return metadata.getSessionFactoryBuilder().build();

        Configuration config = new Configuration();
        sessionFactory = config.configure().buildSessionFactory();

        return sessionFactory;

    }

    public static SessionFactory getSessionFactory()
    {
        return sessionFactory;
    }

    public static void shutdown()
    {
        getSessionFactory().close();
    }


}
