package com.pdarrell.webservices;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pdarrell.Computer;
import com.pdarrell.PageOffset;
import com.pdarrell.Status;
import com.pdarrell.dao.ComputerDAO;
import com.pdarrell.dao.ComputerStatusDAO;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.List;


@Path("/monitor")
public class MonitorService {


    @POST
    @Path("/computerstatuses")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String getComputerStatuses(String pageOffsetjson)
    {
        long offset = 0;
        if (pageOffsetjson != null)
        {
            ObjectMapper mapper = new ObjectMapper();
            PageOffset pageOffset = null;
            try {
                pageOffset = mapper.readValue(pageOffsetjson, PageOffset.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (pageOffset != null) {
                offset = pageOffset.getOffset();
            }
        }

        return ComputerStatusDAO.getComputerStatuses(offset);
    }

    @GET
    @Path("/latestcomputerstatuses")
    @Produces(MediaType.APPLICATION_JSON)
    public String getLastestComputerStatuses()
    {
        String json = "";

        json = ComputerStatusDAO.getLatestComputerStatuses();


        return json;
    }

    @GET
    @Path("/computers")
    @Produces(MediaType.APPLICATION_JSON)
    public String getComputers()
    {
        return ComputerDAO.getComputers();
    }

    @POST
    @Path("/inactivatecomputer")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response inActivateComputer(String json)
    {
        if (!json.equals("")) {
            ObjectMapper mapper = new ObjectMapper();
            Computer computer = null;
            try {
                computer = mapper.readValue(json, Computer.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            ComputerDAO.inActivateComputer(computer);
        }
        return Response.ok().build();
    }

    @GET
    @Path("/numberrows")
    @Produces(MediaType.APPLICATION_JSON)
    public String getComputerStatusesNumberRows()
    {
        return ComputerStatusDAO.getNumberRows();
    }

}
