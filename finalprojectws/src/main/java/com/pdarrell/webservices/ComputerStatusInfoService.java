package com.pdarrell.webservices;

import com.pdarrell.Server;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/computerstatusinfo")
public class ComputerStatusInfoService {

    @POST
    @Path("/computerstatus")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response receiveComputerStatusInfo(String computerStatusJSON)
    {

        if (!computerStatusJSON.equals(""))
        {
            Server.receiveComputerStatus(computerStatusJSON);
        }


        return Response.ok().build();
    }


}
