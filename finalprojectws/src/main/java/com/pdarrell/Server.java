package com.pdarrell;

import com.fasterxml.jackson.databind.*;
import com.pdarrell.dao.DiskDAO;
import com.pdarrell.dao.StatusDAO;

import java.io.IOException;
import java.util.Date;


public class Server {

    public static String receiveComputerStatus(String jsonComputerStatus)
    {
        String error = "";
        ObjectMapper objMapper = new ObjectMapper();
        Status status = null;
        Disk disk = null;
        String returnString = "";
        if (jsonComputerStatus != null && !jsonComputerStatus.equals("")) {
            ComputerStatus computerStatus = null;

            try {
                computerStatus = objMapper.readValue(jsonComputerStatus, ComputerStatus.class);
            } catch (IOException e) {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }

            if (computerStatus != null) {
                Computer computer = new Computer();
                computer.setComputerName(computerStatus.getStatus().getComputer().getComputerName());
                status = computerStatus.getStatus();
                status.setComputer(computer);
                disk = computerStatus.getDisk();
                disk.setComputer(computer);
            }
        }
        returnString = new Date().toString();
        if (status != null) {
            returnString += "\n - " + status.toString();
            //save to database
            StatusDAO.storeStatus(status);
        } else
        {
            error = ("No status information available.");
        }
        if (disk != null)
        {
            disk.setFreeDiskspaceFormatted(DiskspaceFormatter.calculateDiskSpaceString(disk.getFreeDiskspace()));
            returnString += "\n" + disk.toString();
            //save to database
            DiskDAO.storeDisk(disk);
        } else
        {
            error = ("No disk information available.");
        }
        if (!error.equals(""))
        {
            return error;
        }

        return returnString;
    }

}
