package com.pdarrell;

import com.pdarrell.info.ComputerInfo;
import com.pdarrell.info.DiskInfo;
import com.pdarrell.process.ProcessThread;
import com.pdarrell.webservices.client.WebserviceClient;

import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Date;


public class Main {

    private static ProcessThread processThread = null;

    public static void main(String[] args) {

        //shutdown logic
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

            @Override
            public void run() {
                processThread.setExit(true);
                processThread.interrupt();
            }
        }));

        //start process through creating adn starting a runnable as a thread
        processThread = new ProcessThread();
        processThread.start();
        try {
            processThread.join();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


}
