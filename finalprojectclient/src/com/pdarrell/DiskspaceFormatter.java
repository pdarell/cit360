package com.pdarrell;

import java.text.DecimalFormat;

public class DiskspaceFormatter {

    public static String calculateDiskSpaceString(double space)
    {

        if (space <= 0) {
            return "0";
        } else {
            String formattedSpace = "";

            switch ((int)(Math.log10(space)/ Math.log10(1024))) {
                case 5:
                    formattedSpace = new DecimalFormat("#,##0.#").format(space/Math.pow(1024, 5)) + " PB"; //t
                    break;
                case 4:
                    formattedSpace = new DecimalFormat("#,##0.#").format(space/Math.pow(1024, 4)) + " TB"; //g
                    break;
                case 3:
                    formattedSpace = new DecimalFormat("#,##0.#").format(space/Math.pow(1024, 3)) + " GB"; //m
                    break;
                case 2:
                    formattedSpace = new DecimalFormat("#,##0.#").format(space/Math.pow(1024, 2)) + " MB"; //k
                    break;
                case 1:
                    formattedSpace = new DecimalFormat("#,##0.#").format(space/Math.pow(1024, 1)) + " KB"; //b
                    break;
                default :
                    formattedSpace = new DecimalFormat("#,##0.#").format(space) + " B";

            }
            return formattedSpace;
        }
    }

}
