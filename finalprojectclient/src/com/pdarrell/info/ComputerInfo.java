package com.pdarrell.info;

import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class ComputerInfo {

    public static String getComputerName()
    {
        try
        {
            InetAddress addr;
            addr = InetAddress.getLocalHost();
            return addr.getHostName();
        }
        catch (UnknownHostException ex)
        {
            System.out.println("Hostname can not be resolved");
        }
        return "";
    }

    public static String[] getDrives()
    {
        String[] drives = new String[5];
        File[] files = File.listRoots();

        for (int i=0; i < files.length; i++)
        {
            String drivePath = files[i].getPath();
            drives[i] = drivePath.substring(0, (drivePath.length() -1));
        }
        return drives;
    }

}
