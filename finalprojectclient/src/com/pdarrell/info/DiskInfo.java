package com.pdarrell.info;

import java.io.File;

public class DiskInfo {


    public static double getFreeDiskspace(String drive)
    {
        File file = new File(drive);
        return file.getFreeSpace();
    }

    public static double getTotalDiskspace(String drive)
    {
        File file = new File(drive);
        return file.getTotalSpace();
    }

}
