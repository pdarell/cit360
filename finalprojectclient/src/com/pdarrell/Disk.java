package com.pdarrell;



import java.util.Date;


public class Disk {

    private Computer computer = null;

    private String drive = "";


    private long freeDiskspace = 0;


    private String freeDiskspaceFormatted = "";


    private Date scanDateTime = null;


    public Computer getComputer() {
        return computer;
    }

    public void setComputer(Computer computer) {
        this.computer = computer;
    }

    public String getDrive() {
        return drive;
    }

    public void setDrive(String drive) {
        this.drive = drive;
    }

    public long getFreeDiskspace() {
        return freeDiskspace;
    }

    public void setFreeDiskspace(long freeDiskspace) {
        this.freeDiskspace = freeDiskspace;
    }

    public String toString()
    {
        return "Disk: Computer: " + computer.getComputerName() + ", Drive: " + drive + ", free diskspace: " + freeDiskspaceFormatted;
    }

    public String getFreeDiskspaceFormatted() {
        return freeDiskspaceFormatted;
    }

    public void setFreeDiskspaceFormatted(String freeDiskspaceFormatted) {
        this.freeDiskspaceFormatted = freeDiskspaceFormatted;
    }

    public Date getScanDateTime() {
        return scanDateTime;
    }

    public void setScanDateTime(Date scanDateTime) {
        this.scanDateTime = scanDateTime;
    }
}
