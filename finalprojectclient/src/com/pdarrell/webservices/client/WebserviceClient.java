package com.pdarrell.webservices.client;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pdarrell.ComputerStatus;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

import javax.ws.rs.core.MediaType;

public class WebserviceClient {


    public static void sendComputerStatus(String computerStatusJSON)
    {
        if (!computerStatusJSON.equals(""))
        {
            Client client = new Client();

            WebResource webResource = client.resource("http://localhost:8081/finalprojectws_war_exploded/" + "computerstatusinfo" +"/" + "computerstatus");

            ClientResponse response = webResource.accept(MediaType.APPLICATION_JSON)
                    .type(MediaType.APPLICATION_JSON).post(ClientResponse.class, computerStatusJSON);

            if (response != null)
            {

                if (response.getStatus() != 200) {

                   System.out.println("Failed : HTTP error code : "
                    + response.getStatus() + " - " + response.getStatusInfo() + " : " + webResource.toString());
                }

            }

        }
    }




}
