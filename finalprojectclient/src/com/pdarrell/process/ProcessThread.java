package com.pdarrell.process;

import com.pdarrell.Client;
import com.pdarrell.ComputerStatus;
import com.pdarrell.Output;
import com.pdarrell.info.ComputerInfo;
import com.pdarrell.info.DiskInfo;
import com.pdarrell.webservices.client.WebserviceClient;

import java.util.Date;

public class ProcessThread extends Thread implements Runnable{

    private boolean exit = false;

    public ProcessThread ()
    {

    }


    @Override
    public void run() {

        while (!exit)
        {
            Date curDate = new Date();
            String computerName = ComputerInfo.getComputerName();

            String[] drives = ComputerInfo.getDrives();
            for (String drive : drives) {
                if (drive != null && !drive.equals("")) {
                    Double freeDiskspace = DiskInfo.getFreeDiskspace(drive);
                    ComputerStatus computerStatus = Client.createComputerStatus(computerName, "Active", curDate, drive, freeDiskspace.longValue(), false);
                    String json = Client.sendComputerStatus(computerStatus);
                    WebserviceClient.sendComputerStatus(json);
                }
            }
            Thread.yield();
            try {
                Thread.sleep(360000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public void setExit(boolean exit)
    {
        this.exit = exit;
    }



}
